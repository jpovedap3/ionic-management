# Introduction

Ionic-management is a sample project developed with Ionic 5 (Angular) that implements a working solution for Android, iOS and PWA.
The project is completely functional and is integrated with an external API created in Firebase's Firestore.

## Internacionalization

At the moment, it succesfully implements ngx-translate, but there are only two key-value pairs in the dictionaries.
Completely translate the app is a time-consuming issue with no current value, so it has been postponed.

## Data persistance - Backend

The data is stored in a non-relational DB.
The engine is Firebase's Firestore, and it implements rules that ensure the data integrity.

## Data persistance - API Integration

The front app consumes firestore's services thanks to the use of the methods present in the firebase's plugin.
All this logic is present in the service provider.

# Instructions

Once downloaded, run this command to install dependencies' packages:

**$ npm install**

To launch the web project, we run:

**$ ionic serve**

## Android
To build Android version:

**$ ionic cordova build android**

Then you can run it opening platforms/android with AndroidStudio.

## iOS
To build Android version:

**$ ionic cordova build android**

Then you can run it opening platforms/ios with XCode.

## Firebase Hosting / PWA

We need to install firebase-tools:

**$ npm install -g firebase-tools**

Then login as an user with access to the firebase's console configured in environment.firebaseConfig:

**$ firebase login**

And select the hosting project which we want to update:

**$ firebase use projectWeWantToUpdate**

Now we need to create a PWA instance optimized for production:

**$ ionic build --prod**

This command generates a /www that contains the exportable webproject.

Finally we deploy the www in the host

**$ firebase deploy**

[Live demo here](https://manage-employees-f67d2.web.app)