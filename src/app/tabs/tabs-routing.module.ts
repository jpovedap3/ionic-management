import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'employees',
        loadChildren: () => import('../employees-list/employees-list.module').then(m => m.EmployeesListPageModule)
      },
      {
        path: 'department/:departmentId',
        loadChildren: () => import('../employees-list/employees-list.module').then(m => m.EmployeesListPageModule)
      },
      {
        path: 'departments',
        loadChildren: () => import('../departments-list/departments-list.module').then(m => m.DepartmentsListPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/employees',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/employees',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
