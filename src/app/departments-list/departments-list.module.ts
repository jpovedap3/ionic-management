import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DepartmentsListPageRoutingModule } from './departments-list-routing.module';

import { DepartmentsListPage } from './departments-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DepartmentsListPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [DepartmentsListPage]
})
export class DepartmentsListPageModule {}
