import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DepartmentsListPage } from './departments-list.page';

const routes: Routes = [
  {
    path: '',
    component: DepartmentsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DepartmentsListPageRoutingModule {}
