import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseApiService } from '../services/firebase-api.service';
import Swal from 'sweetalert2'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Department } from '../interfaces/interfaces';

@Component({
  selector: 'app-departments-list',
  templateUrl: './departments-list.page.html',
  styleUrls: ['./departments-list.page.scss'],
})
export class DepartmentsListPage implements OnInit {
  departments: Array<Department>;
  departmentForm: FormGroup;
  validationMessages = [
    { type: 'required', message: 'This field is required.' },
    { type: 'minlength', message: 'This field must be at least 2 characters long.' },
    { type: 'maxlength', message: 'This field cannot be more than 25 characters long.' }
  ];
  constructor(private router: Router, public firebase: FirebaseApiService) { }

  ngOnInit() {
    this.initInputs();
    this.firebase.read('departments').subscribe((departmentsReferences) => {
      this.departments = departmentsReferences.map(d => {
        let data = d.payload.doc.data() as any;
        return { id: d.payload.doc.id, name: data.name };
      });
    });
  }

  initInputs() {
    this.departmentForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(25),
      Validators.minLength(2)])
    });
  }

  seeDepartment(department) {
    this.router.navigate(['/tabs/department/' + department.id], { state: department });
  }

  deleteDepartment(department) {
    console.log(department);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      this.firebase.delete('departments', department.id).then(() => {
        Swal.fire(
          'Success',
          'The department has been deleted',
          'success'
        );
      }, err => {
        Swal.fire(
          'Error',
          err,
          'error'
        );
      })
    });
  }

  submitForm() {
    console.log(this.departmentForm.value);
    if (this.departmentForm.valid) {
      this.firebase.create('departments', this.departmentForm.value).then(() => {
        Swal.fire({
          title: 'Success',
          text: "The department's data has been saved",
          icon: 'success'
        });
      }, err => {
        console.log("An error happened", err);
      });
    }
  }
}
