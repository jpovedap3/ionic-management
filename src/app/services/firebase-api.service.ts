import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FilterCondition } from '../interfaces/interfaces';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FirebaseApiService {

  constructor(private af: AngularFirestore) { }


  public create(collection, data) {
    return this.af.collection(collection).add(data);
  }

  public read(collection, orderBy?, filterCondition?: Array<FilterCondition>) {
    let collectionRef = this.af.collection(collection);
    let query: firebase.firestore.Query = collectionRef.ref
    if (orderBy) {
      query = query.orderBy(orderBy.key, orderBy.direction);
    }
    if (filterCondition){
      filterCondition.forEach(f => {
        if(f.value){
          query = query.where(f.key, f.operator, f.value);
        }
      })
    }
    return this.af.collection(collection, ref => { return query }).snapshotChanges();
  }

  public readDocument(collection, doc) {
    return this.af.collection(collection).doc(doc).snapshotChanges();
  }

  public delete(collection, documentId) {
    return this.af.collection(collection).doc(documentId).delete();
  }

  public updateField(collection, documentId, data) {
    return this.af.collection(collection).doc(documentId).update(data);
  }

  public getNewFirestoreTimestamp(date?) {
    return firebase.firestore.Timestamp.fromDate(date ? date : new Date());
  }
}
