import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FirebaseApiService } from '../services/firebase-api.service';
import { Employee, Department } from '../interfaces/interfaces';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.page.html',
  styleUrls: ['./create-employee.page.scss'],
})
export class CreateEmployeePage implements OnInit {
  employeeForm: FormGroup;
  employee: Employee;
  departments: Array<Department>;
  title: string = "Añadir empleado";
  validationMessages = [
    { type: 'required', message: 'This field is required.' },
    { type: 'minlength', message: 'This field must be at least 2 characters long.' },
    { type: 'maxlength', message: 'This field cannot be more than 25 characters long.' },
    { type: 'min', message: 'You have to be an adult to get employed' }
  ];

  constructor(private router: Router, public firebase: FirebaseApiService, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getDepartments();
    this.setupEditOrCreate();
  }

  getDepartments(){
    this.firebase.read('departments').subscribe(departmentsRef => {
      this.departments = departmentsRef.map(d => {
        let data = d.payload.doc.data() as any;
        return { id: d.payload.doc.id, name: data.name };
      });
    });
  }

  setupEditOrCreate(){
    if (this.actRoute.snapshot.params['id']) {
      this.title = "Editar empleado";
      this.actRoute.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.employee = this.router.getCurrentNavigation().extras.state as Employee;
          this.employee.id = this.actRoute.snapshot.params['id'];
          this.initInputs();
        } else {
          this.retrieveEmployee(this.actRoute.snapshot.params['id']);
        }
      });
    } else {
      this.initInputs();
    }
  }

  initInputs() {
    this.employeeForm = new FormGroup({
      name: new FormControl(this.employee ? this.employee.name : '', [Validators.required, Validators.maxLength(25),
      Validators.minLength(2)]),
      lastname: new FormControl(this.employee ? this.employee.lastname : '', [Validators.required, Validators.maxLength(25),
      Validators.minLength(2)]),
      age: new FormControl(this.employee ? this.employee.age : '', [Validators.required, Validators.min(18)]),
      department: new FormControl(this.employee ? this.employee.department : '',Validators.required),
    });
  }

  retrieveEmployee(employeeId) {
    this.firebase.readDocument('employees', employeeId).subscribe(employee => {
      this.employee = employee.payload.data() as Employee;
      this.employee.id = employeeId;
      this.initInputs();
    })
  }

  submitForm() {
    if (this.employeeForm.valid) {
      console.log(this.employeeForm.value);
      this.employee ? this.editEmployee() : this.createEmployee();
    }
  }

  createEmployee() {
    let newEmployee: Employee = this.employeeForm.value;
    newEmployee.initDate = this.firebase.getNewFirestoreTimestamp();
    this.firebase.create('employees', this.employeeForm.value).then(() => {
      Swal.fire({
        title: 'Success',
        text: "The employee's data has been saved",
        icon: 'success'
      });
      this.router.navigate(['']);
    }, err => {
      console.log("An error happened", err);
    });
  }

  editEmployee() {
    this.firebase.updateField('employees', this.employee.id, this.employeeForm.value).then(() => {
      Swal.fire({
        title: 'Success',
        text: "The employee's data has been saved",
        icon: 'success'
      });
      this.router.navigate(['']);
    }, err => {
      console.log("An error happened", err);
    });
  }
}
