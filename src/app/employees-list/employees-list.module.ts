import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EmployeesListPage } from './employees-list.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { EmployeesListPageRoutingModule } from './employees-list-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    EmployeesListPageRoutingModule
  ],
  declarations: [EmployeesListPage]
})
export class EmployeesListPageModule {}
