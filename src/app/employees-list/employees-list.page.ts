import { Component } from '@angular/core';
import { Employee, FilterCondition, Department } from '../interfaces/interfaces';
import { FirebaseApiService } from '../services/firebase-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-employees-list',
  templateUrl: 'employees-list.page.html',
  styleUrls: ['employees-list.page.scss']
})
export class EmployeesListPage {
  employees: Array<Employee>;
  department: Department;
  datetimeString: string;
  oldToNew: boolean = true;
  filterByDepartment: FilterCondition;

  constructor(private router: Router, public firebase: FirebaseApiService, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.actRoute.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.department = this.router.getCurrentNavigation().extras.state as Department;
      }
    });
    this.filterByDepartment = { key: "department", operator: "==", value: this.actRoute.snapshot.params['departmentId'] };
    this.getEmployees();
  }

  getEmployees() {
    let filters = [this.filterByDepartment];
    if(this.datetimeString){
      filters.push({ key: "initDate", operator: ">=", value: this.firebase.getNewFirestoreTimestamp(new Date(this.datetimeString.split('T')[0]))})
    }
    let orderBy = { key: "initDate", direction: this.oldToNew ? 'asc' : 'desc' };
    this.firebase.read('employees', orderBy, filters).subscribe((employeesReferences) => {
      this.employees = employeesReferences.map(e => {
        let data = e.payload.doc.data() as any;
        data.id = e.payload.doc.id;
        return data;
      });
    });
  }

  editEmployee(employee) {
    this.router.navigate(['/edit-employee/' + employee.id], { state: employee });
  }

  deleteEmployee(employee) {
    console.log(employee);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      this.firebase.delete('employees', employee.id).then(() => {
        Swal.fire(
          'Success',
          'The employee has been deleted',
          'success'
        );
      }, err => {
        Swal.fire(
          'Error',
          err,
          'error'
        );
      })
    });
  }

  sortByDate() {
    this.oldToNew = !this.oldToNew;
    this.getEmployees();
  }
}
