export interface Employee {
    id: string,
    name: string,
    lastname: string,
    age: number,
    initDate: any,
    department: string
}

export interface Department {
    id: string,
    name: string
}

export interface FilterCondition {
    key: string,
    operator: any,
    value: any
}