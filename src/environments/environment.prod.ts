export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyC6tmvmNQcUfeV6rb13DJ9u0YW5wDbYR8I",
    authDomain: "manage-employees-f67d2.firebaseapp.com",
    databaseURL: "https://manage-employees-f67d2-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "manage-employees-f67d2",
    storageBucket: "manage-employees-f67d2.appspot.com",
    messagingSenderId: "965167368363",
    appId: "1:965167368363:web:494a07c5ed0310badd8fee",
    measurementId: "G-CGRJ1MFSSC"
  }
};
