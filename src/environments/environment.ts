// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC6tmvmNQcUfeV6rb13DJ9u0YW5wDbYR8I",
    authDomain: "manage-employees-f67d2.firebaseapp.com",
    databaseURL: "https://manage-employees-f67d2-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "manage-employees-f67d2",
    storageBucket: "manage-employees-f67d2.appspot.com",
    messagingSenderId: "965167368363",
    appId: "1:965167368363:web:494a07c5ed0310badd8fee",
    measurementId: "G-CGRJ1MFSSC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
